package aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.activities.DetailsActivity
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.clases_constructor.Pelicula
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.R
import kotlinx.android.synthetic.main.item_pelicula.view.*

class PeliculasAdapter(val items : List<Pelicula>, val context : Context ) : RecyclerView.Adapter<PeliculasAdapter.PeliculasViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeliculasViewHolder {
        return PeliculasViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pelicula, parent, false))
    }

    override fun onBindViewHolder(holder: PeliculasViewHolder, position: Int) {
        val pelicula = items.get(position)

        holder.tvNombrePelicula?.text = pelicula.nombre
        holder.tvAnhoPelicula?.text = pelicula.anho.toString()

        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra("MasDatos", pelicula);
            context.startActivity(intent)
        }



    }

    class PeliculasViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvNombrePelicula = view.tvNombrePelicula
        val tvAnhoPelicula = view.tvAnho


    }
}