package aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.activities


import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.clases_constructor.Pelicula
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.R
import kotlinx.android.synthetic.main.activity_detalles.*

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles)



        val pelicula: Pelicula = intent.getSerializableExtra("MasDatos" ) as Pelicula
        tvTitulo.text = pelicula.nombre
        tvDatos.text ="Director : " +  pelicula.director.nombre +"\n \n" + "Teléfono : " + pelicula.director.telefono
        tvDatos2.text ="País : " + pelicula.pais + "\n \n" + "Año : " + pelicula.anho + "\n \n " + "Duración : " + pelicula.duracion.toString()
        tvSinopsis.text = "Sinopsis : " + pelicula.sinopsis
        ivPortada.setImageResource(pelicula.imagen)


        btnEdit.setOnClickListener {
            val intent = Intent(this, ModificarActivity::class.java)
            intent.putExtra("DatosSob", pelicula )
            startActivity(intent)
        }
        btnDel.setOnClickListener{

            val builder = AlertDialog.Builder(this)

            builder.setTitle("Eliminar película.")


            builder.setMessage("Desea eliminar la película?")


            builder.setPositiveButton("Si"){dialog, which ->

                Toast.makeText(applicationContext,"Eliminando película...", Toast.LENGTH_SHORT).show()

                finish()

            }

            builder.setNegativeButton("No"){dialog,which ->
            }

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }
}