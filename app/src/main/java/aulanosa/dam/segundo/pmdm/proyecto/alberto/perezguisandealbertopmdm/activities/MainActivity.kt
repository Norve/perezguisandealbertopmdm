package aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.R
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.listas.ListaActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnAcceder.setOnClickListener {
            val intent = Intent(this, ListaActivity::class.java)
            startActivity(intent)
        }
        btnInfo.setOnClickListener{
            val intent = Intent ( this, InfoActivity::class.java)
            startActivity(intent)
        }

    }
}

