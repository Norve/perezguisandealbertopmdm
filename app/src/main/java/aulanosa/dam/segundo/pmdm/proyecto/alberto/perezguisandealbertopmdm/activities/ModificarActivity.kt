package aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.activities

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.R
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.clases_constructor.Pelicula
import kotlinx.android.synthetic.main.activity_modificarlista.*
import java.io.Serializable

class ModificarActivity : AppCompatActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modificarlista)

        val serial: Serializable? = intent.getSerializableExtra("DatosSob")
        if(serial != null) {
                //Integer.toString(anho)

                val pelicula: Pelicula = serial as Pelicula
                etTitulo.setText(pelicula.nombre)
                etAnho.setText( pelicula.anho.toString())
                etDirector.setText( pelicula.director.nombre)
                etTelefono.setText(pelicula.director.telefono)
                etPais.setText(pelicula.pais)
                etDuracion.setText(pelicula.duracion.toString())
                etSinopsis.setText(pelicula.sinopsis)
            }
        btnAdd.setOnClickListener{
            Toast.makeText(applicationContext,"Añadiendo película...", Toast.LENGTH_SHORT).show()

            finish()

        }
        btnDes.setOnClickListener{

            val builder = AlertDialog.Builder(this)

            builder.setTitle("Deshacer cambios.")


            builder.setMessage("Desea deshacer los cambios?")


            builder.setPositiveButton("Si"){dialog, which ->

                Toast.makeText(applicationContext,"Deshaciendo cambios...", Toast.LENGTH_SHORT).show()

                finish()
            }

            builder.setNegativeButton("No"){dialog,which ->

            }

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }
}