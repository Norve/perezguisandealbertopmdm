package aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.listas

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.clases_constructor.Director
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.activities.ModificarActivity
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.clases_constructor.Pelicula
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.R
import aulanosa.dam.segundo.pmdm.proyecto.alberto.perezguisandealbertopmdm.adapters.PeliculasAdapter
import kotlinx.android.synthetic.main.activity_lista.*

class ListaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista)
        rvLista.layoutManager = LinearLayoutManager(this);

        btnModif.setOnClickListener {
            val intent = Intent(this, ModificarActivity::class.java)
            startActivity(intent)
        }

        rvLista.adapter = PeliculasAdapter(listaPeliculas(), this);
    }
    fun listaPeliculas (): List<Pelicula> {
        val d1 = Director(1, "Zack Snyder", "654334455")
        val d2 = Director(2, "Ibai Llanos", "633423211")
        val d3 = Director(3, "Sylvain White", "722345432")
        val lista = ArrayList<Pelicula>()
        val p1 = Pelicula(1, "Batman vs. Superman: El origen de la justicia", d1, 2016, "Fearing that the actions of Superman are left unchecked, Batman takes on the Man of Steel, while the world wrestles with what kind of a hero it really needs. ", "EEUU", R.drawable.img1, 122)
        val p2 = Pelicula(2, "League Of Legends 2018 World Championship Finals - Live From Korea", d2, 2018, "Vive la final del mundial de League of Legends en la gran pantalla. Verás todo el streaming en directo y el emocionante momento en el que el ganador levante la Copa del Invocador.", "Korea", R.drawable.img3, 240)
        val p3 = Pelicula(3, "Slenderman", d3, 2018, " En un pequeño pueblo de Massachusetts, cuatro estudiantes de instituto llevan a cabo un ritual en un intento de desmentir la leyenda de SLENDER MAN. Cuando una de las chicas desaparece misteriosamente, empiezan a sospechar que ella puede ser su última víctima. ", "EEUU", R.drawable.img2, 110)
        lista.add(p1)
        lista.add(p2)
        lista.add(p3)
        return lista
    }


}

